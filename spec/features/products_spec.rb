require 'rails_helper'

RSpec.feature 'Products', type: :feature do
  describe 'GET #show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:no_related_product) { create(:product) }
    let!(:related_products) { create_list(:product, Constants::DISPLAY_RELATED_PRODUCTS_COUNT + 1, taxons: [taxon]) }

    before do
      visit potepan_product_path(product.id)
    end

    it '正しい情報が表示されること' do
      within '.page-title' do
        expect(page).to have_content product.name
      end
      within '.media-body h2' do
        expect(page).to have_content product.name
      end
      within '.media-body p' do
        expect(page).to have_content product.description
      end
      within '.media-body h3' do
        expect(page).to have_content product.display_price.to_s
      end
    end

    it '正しい関連情報を表示すること' do
      within '.productsContent' do
        # 関連商品が4件表示されること
        Constants::DISPLAY_RELATED_PRODUCTS_COUNT.times do |i|
          expect(page).to have_content related_products[i].name
          expect(page).to have_content related_products[i].price
        end
        # 関連商品が5件目が表示されないこと
        expect(page).to have_no_content related_products.last.name
        # 関連商品の領域に非関連商品が表示されないこと
        expect(page).to have_no_content no_related_product.name
      end
    end

    it 'HeaderのLogo押下で、トップ画面に遷移すること' do
      click_link 'logo'
      expect(current_path).to eq potepan_path
    end

    it 'HeaderのHome押下で、トップ画面に遷移すること' do
      within '.header .nav' do
        click_on 'Home'
      end
      expect(current_path).to eq potepan_path
    end

    it 'Light SectionのHome押下で、トップ画面に遷移すること' do
      within '.lightSection .breadcrumb' do
        click_on 'Home'
      end
      expect(current_path).to eq potepan_path
    end

    it '一覧ページへ戻るを押下で、カテゴリーページに遷移すること' do
      click_link '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path taxon.id
    end

    it '関連商品のリンクが正常に動作すること' do
      within '.productsContent' do
        click_on related_products[0].name
        expect(current_path).to eq potepan_product_path(related_products[0].id)
      end
    end
  end
end
