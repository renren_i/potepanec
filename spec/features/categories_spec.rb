require 'rails_helper'

RSpec.feature 'categories', type: :feature do
  describe 'GET #show' do
    let(:taxonomy)  { create(:taxonomy) }
    let(:taxon)     { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy, name: 'Bags') }
    let(:taxon2)    { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
    let!(:product)  { create(:product, taxons: [taxon]) }
    let!(:product2) { create(:product, taxons: [taxon2]) }

    before do
      visit potepan_category_path(taxon.id)
    end

    it 'カテゴリー別商品一覧で必要な情報のみを表示' do
      expect(page).to have_content taxonomy.name
      within '.page-title' do
        expect(page).to have_content taxon.name
      end
      within '.breadcrumb' do
        expect(page).to have_content taxon.name
      end
      within '.productCaption' do
        expect(page).to have_content product.name
        expect(page).to have_no_content product2.name
      end
      within '.productCaption' do
        expect(page).to have_content product.display_price.to_s
      end
    end

    it 'light_sectionのHome押下で、トップ画面に遷移すること' do
      within '.breadcrumb' do
        click_on 'Home'
      end
      expect(current_path).to eq potepan_path
    end

    it '商品を押下で、商品詳細画面に遷移すること' do
      first(:link, product.name).click
      expect(current_path).to eq potepan_product_path product.id
    end

    it '商品カテゴリーのtaxonを押下で、該当するtaxonのページに遷移すること' do
      within '.panel-body .navbar-collapse' do
        click_on taxon2.name, match: :first
      end
      expect(current_path).to eq potepan_category_path taxon2.id
    end
  end
end
