require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'ページタイトル' do
    it '引数ブランクの時、店名のみ表示されること' do
      expect(helper.title_name('')).to eq('BIGBAG Store')
    end

    it '引数がnilの時、店名のみ表示されること' do
      expect(helper.title_name(nil)).to eq('BIGBAG Store')
    end

    it '商品名 - 店名が表示されること' do
      expect(helper.title_name('Test')).to eq('Test - BIGBAG Store')
    end
  end
end
