require 'rails_helper'

RSpec.describe 'ProductDecorator', type: :model do
  let!(:taxon1) { create(:taxon) }
  let!(:taxon2) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon1, taxon2]) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon1, taxon2]) }

  describe 'related_products' do
    it '関連商品を含むこと' do
      expect(product.products_in_same_taxon).to include(*related_products)
    end

    it 'product自体を関連商品に含まないこと' do
      expect(product.products_in_same_taxon).not_to include product
    end

    it '関連商品を重複しないこと' do
      expect(related_products).to eq related_products.uniq
    end
  end
end
