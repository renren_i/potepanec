require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET #show' do
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it 'showをレンダリングすること' do
      expect(response).to be_successful
      expect(response).to have_http_status 200
      expect(response).to render_template :show
    end

    it 'taxonを@taxonに割り当てられていること' do
      expect(assigns(:taxon)).to eq(taxon)
    end

    it 'taxonomyを@taxonomyに割り当てられていること' do
      expect(assigns(:taxonomies)).to eq([taxonomy])
    end

    it 'productを@productに割り当てられていること' do
      expect(assigns(:products)).to eq([product])
    end
  end
end
