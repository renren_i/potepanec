require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, Constants::DISPLAY_RELATED_PRODUCTS_COUNT + 1, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it 'showをレンダリングすること' do
      expect(response).to be_successful
      expect(response).to have_http_status 200
      expect(response).to render_template :show
    end

    it 'リクエストされたproductを@productに割り当てられていること' do
      expect(assigns(:product)).to eq(product)
    end

    it '4つの関連商品が存在する' do
      expect(assigns(:related_products).count).to eq Constants::DISPLAY_RELATED_PRODUCTS_COUNT
    end
  end
end
