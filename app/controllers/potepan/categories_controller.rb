class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.includes(:taxons)
    @products = Spree::Product.in_taxon(@taxon).with_master_images_and_default_price
  end
end
