class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.products_in_same_taxon.with_master_images_and_default_price.limit(Constants::DISPLAY_RELATED_PRODUCTS_COUNT)
  end
end
