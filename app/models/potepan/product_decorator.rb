Spree::Product.class_eval do
  scope :with_master_images_and_default_price, lambda {
    includes(master: %i[images default_price])
  }

  def products_in_same_taxon
    Spree::Product.in_taxons(taxons).where.not(id: id).distinct
  end
end
