module ApplicationHelper
  def title_name(add_title = '')
    if add_title.present?
      "#{add_title} - #{Constants::STORE_NAME}"
    else
      Constants::STORE_NAME
    end
  end
end
